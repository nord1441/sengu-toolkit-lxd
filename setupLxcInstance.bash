HOST="$1"
USER="$2"
ENCR="$3"
SHARE="$4"
CRDSETUPCMD="$5"
CRDPIN="$6"
BRANCH="$7"

lxc launch ubuntu:20.04 "$HOST"
lxc exec "$HOST" -- useradd -m -p "$ENCR" -s /bin/bash "$USER"
lxc exec "$HOST" -- usermod -a -G sudo "$USER"
lxc exec "$HOST" -- sed -e s/%sudo\.\*\.ALL\=\(ALL\:ALL\)\.\*\.ALL/%sudo\ ALL\=\(ALL\:ALL\)\ NOPASSWD\:ALL/g -i /etc/sudoers
lxc file push ./debconf.expect "$HOST"/home/"$USER"/
sleep 3
lxc exec "$HOST" -- apt-get update
sleep 3
lxc exec "$HOST" -- apt-get install -y expect
lxc exec "$HOST" -- expect /home/"$USER"/debconf.expect
sleep 3
lxc exec "$HOST" -- su $USER -c "git -C /tmp clone https://nord1441@bitbucket.org/nord1441/sengu-toolkit.git -b $BRANCH"
lxc exec "$HOST" -- su $USER -c "bash /tmp/sengu-toolkit/set-crd-credential.sh -C \"$CRDSETUPCMD\" -P $CRDPIN"
lxc exec "$HOST" -- su $USER -c "bash /tmp/sengu-toolkit/autorun.sh -C"
hostid=`getent group "$USER" | cut -f 3 -d:`
guestid=`lxc exec "$HOST" -- su "$USER" -c "getent group $USER | cut -f 3 -d:"`
lxc config set "$HOST" raw.idmap "both $hostid $guestid"
lxc config device add "$HOST" syncthing disk source="$SHARE"/Sync path=/home/"$USER"/Sync/
lxc config device add "$HOST" downloads disk source="$SHARE"/Downloads path=/home/"$USER"/Downloads/
lxc restart "$HOST"
