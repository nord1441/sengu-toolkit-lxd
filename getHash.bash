PASS="$1"

SALT=`cat /dev/urandom | tr -dc 'a-zA-Z0-9./' | fold -w 2 | head -n 1`
ENCR=`./crypt "$PASS" "$SALT"`
echo "$ENCR"
