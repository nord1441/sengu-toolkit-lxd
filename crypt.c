#include <stdio.h>
#include <unistd.h>
#include <crypt.h>

int main(int argc, char ** argv) {
	if(argc != 3) {
		return -1;
		fprintf(stderr, "Not a valid args.\n");
	}
	char * encrypted = crypt(argv[1], argv[2]);
	fprintf(stdout, "%s", encrypted);
	return 0;
}
